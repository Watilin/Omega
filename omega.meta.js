// ==UserScript==
// @name          Omega
// @namespace     fr.kergoz-panic.watilin
// @description   UI enhancing script focused on user efficiency and minimum HTTP traffic.
//
// @match         http://*.ogame.gameforge.com/game/index.php?*
// @exclude       *?*page=empire*
//
// @grant         GM_getValue
// @grant         GM_setValue
// @grant         GM_getResourceText
// @grant         GM_getResourceURL
//
// @version       1.3
// @icon          https://raw.githubusercontent.com/Watilin/Omega/v1.3/icon.png
// @resource      style    https://raw.githubusercontent.com/Watilin/Omega/v1.3/omega.css
// @resource      sprite   https://raw.githubusercontent.com/Watilin/Omega/v1.3/sprite.png
// @resource      guide    https://raw.githubusercontent.com/Watilin/Omega/v1.3/guide.html
//
// @homepage      https://github.com/Watilin/Omega#omega
// @author        Watilin
// @copyright     2013+, Watilin
// @license       Creative Commons 3.0 BY-NC-SA
//
// @downloadURL   https://raw.githubusercontent.com/Watilin/Omega/master/omega.user.js
// @updateURL     https://raw.githubusercontent.com/Watilin/Omega/master/omega.meta.js
// ==/UserScript==
